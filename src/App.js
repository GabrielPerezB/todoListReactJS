import React, { Component } from 'react';

import './styles/App.css';

import TodoList from './components/TodoList';
import TodoBox from './components/TodoBox';

class App extends Component {
  constructor(){
    super();

    this.state = {
      todoItems: []
    };

    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(e){
    let todoListValue = this.refs.todobox.getInputValue();
    let newTodoItemsValue = this.state.todoItems.concat(todoListValue)
    this.setState({todoItems: newTodoItemsValue});
  }
  render() {
    return (
      <div className="App">
        <h1 className="App-Title">Todo List</h1>
        <TodoBox  ref="todobox" onSubmit={this.onSubmit}/>
        <TodoList  ref="todolist" items={this.state.todoItems}/>
      </div>
    );
  }
}

export default App;
